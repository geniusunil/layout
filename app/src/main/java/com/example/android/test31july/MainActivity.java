package com.example.android.test31july;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<Movie> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private MoviesAdapter mAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mAdapter = new MoviesAdapter(movieList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
//        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 16));
        // set the adapter
        recyclerView.setAdapter(mAdapter);

        prepareMovieData();
        }

    private void prepareMovieData() {
        //Top line with 3 textViews

        String text = "<font color=#02629c>Title: </font> <font color=#000000>UI/UX Designer</font>";
        TextView yourtextview=(TextView) findViewById(R.id.hello1);
        yourtextview.setText(Html.fromHtml(text));

        text=  "<font color=#02629c>City: </font> <font color=#000000>Chandigarh</font>";
        yourtextview=(TextView) findViewById(R.id.hello2);
        yourtextview.setText(Html.fromHtml(text));

        text=  "<font color=#02629c>State: </font> <font color=#000000>Chandigarh</font>";
        yourtextview=(TextView) findViewById(R.id.hello3);
        yourtextview.setText(Html.fromHtml(text));

        Movie movie = new Movie();
        movieList.add(movie);

        movie = new Movie();
        movieList.add(movie);

        movie = new Movie();
        movieList.add(movie);

        movie = new Movie();
        movieList.add(movie);

        movie = new Movie();
        movieList.add(movie);

        movie = new Movie();
        movieList.add(movie);

        movie = new Movie();
        movieList.add(movie);

        movie = new Movie();
        movieList.add(movie);

        movie = new Movie();
        movieList.add(movie);
/*

        movie = new Movie("Inside Out", "Animation, Kids & Family", "2015");
        movieList.add(movie);

        movie = new Movie("Star Wars: Episode VII - The Force Awakens", "Action", "2015");
        movieList.add(movie);

        movie = new Movie("Shaun the Sheep", "Animation", "2015");
        movieList.add(movie);

        movie = new Movie("The Martian", "Science Fiction & Fantasy", "2015");
        movieList.add(movie);

        movie = new Movie("Mission: Impossible Rogue Nation", "Action", "2015");
        movieList.add(movie);

        movie = new Movie("Up", "Animation", "2009");
        movieList.add(movie);

        movie = new Movie("Star Trek", "Science Fiction", "2009");
        movieList.add(movie);

        movie = new Movie("The LEGO Movie", "Animation", "2014");
        movieList.add(movie);

        movie = new Movie("Iron Man", "Action & Adventure", "2008");
        movieList.add(movie);

        movie = new Movie("Aliens", "Science Fiction", "1986");
        movieList.add(movie);

        movie = new Movie("Chicken Run", "Animation", "2000");
        movieList.add(movie);

        movie = new Movie("Back to the Future", "Science Fiction", "1985");
        movieList.add(movie);

        movie = new Movie("Raiders of the Lost Ark", "Action & Adventure", "1981");
        movieList.add(movie);

        movie = new Movie("Goldfinger", "Action & Adventure", "1965");
        movieList.add(movie);

        movie = new Movie("Guardians of the Galaxy", "Science Fiction & Fantasy", "2014");
        movieList.add(movie);
*/

        mAdapter.notifyDataSetChanged();
    }
}

